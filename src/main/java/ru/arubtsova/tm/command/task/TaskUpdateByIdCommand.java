package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

public class TaskUpdateByIdCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-update-by-id";
    }

    @Override
    public String description() {
        return "update a task by id.";
    }

    @Override
    public void execute() {
        System.out.println("Task:");
        System.out.println("Enter Task Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneById(id);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("Enter New Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter New Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdate = serviceLocator.getTaskService().updateTaskById(id, name, description);
        if (taskUpdate == null) throw new TaskNotFoundException();
        System.out.println("Task was successfully updated");
    }

}
