package ru.arubtsova.tm.command.task;

import ru.arubtsova.tm.command.AbstractTaskCommand;
import ru.arubtsova.tm.exception.entity.TaskNotFoundException;
import ru.arubtsova.tm.model.Task;
import ru.arubtsova.tm.util.TerminalUtil;

public class TaskFinishByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "change task status to Complete by task name.";
    }

    @Override
    public void execute() {
        System.out.println("Task:");
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

}
