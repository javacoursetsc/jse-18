package ru.arubtsova.tm.command.authorization;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.model.User;

public class UserViewProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "view-profile";
    }

    @Override
    public String description() {
        return "show your profile information.";
    }

    @Override
    public void execute() {
        final User user = serviceLocator.getAuthService().getUser();
        System.out.println("Profile Overview:");
        System.out.println("Login: " + user.getLogin());
        System.out.println("Email: " + user.getEmail());
        System.out.println("First Name: " + user.getFirstName());
        System.out.println("Last Name: " + user.getLastName());
        System.out.println("Middle Name: " + user.getMiddleName());
    }

}
