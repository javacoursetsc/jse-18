package ru.arubtsova.tm.command.authorization;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.model.User;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserChangePasswordCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "change-password";
    }

    @Override
    public String description() {
        return "change password for current user";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Change Password:");
        System.out.println("Enter New Password");
        final String newPassword = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().setPassword(userId, newPassword);
        System.out.println("Password was successfully updated");
    }

}
