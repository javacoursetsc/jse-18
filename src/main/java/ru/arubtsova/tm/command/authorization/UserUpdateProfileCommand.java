package ru.arubtsova.tm.command.authorization;

import ru.arubtsova.tm.command.AbstractCommand;
import ru.arubtsova.tm.exception.entity.UserNotFoundException;
import ru.arubtsova.tm.model.User;
import ru.arubtsova.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "update-profile";
    }

    @Override
    public String description() {
        return "add your name to your profile.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("Update Profile:");
        System.out.println("Enter First Name:");
        final String firstName = TerminalUtil.nextLine();
        System.out.println("Enter Last Name:");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("Enter Middle Name:");
        final String middleName = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().updateUser(userId, firstName, lastName, middleName);
        if (user == null) throw new UserNotFoundException();
        System.out.println("Profile was successfully updated");
    }

}
