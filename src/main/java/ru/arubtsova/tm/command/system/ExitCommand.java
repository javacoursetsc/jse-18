package ru.arubtsova.tm.command.system;

import ru.arubtsova.tm.command.AbstractCommand;

public class ExitCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String description() {
        return "close application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}