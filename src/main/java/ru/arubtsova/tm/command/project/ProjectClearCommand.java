package ru.arubtsova.tm.command.project;

import ru.arubtsova.tm.command.AbstractProjectCommand;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-clear";
    }

    @Override
    public String description() {
        return "delete all projects.";
    }

    @Override
    public void execute() {
        System.out.println("Project Clear:");
        serviceLocator.getProjectService().clear();
    }

}
