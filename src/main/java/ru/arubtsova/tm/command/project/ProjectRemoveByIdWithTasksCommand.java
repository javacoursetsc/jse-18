package ru.arubtsova.tm.command.project;

import ru.arubtsova.tm.command.AbstractProjectCommand;
import ru.arubtsova.tm.exception.entity.ProjectNotFoundException;
import ru.arubtsova.tm.model.Project;
import ru.arubtsova.tm.util.TerminalUtil;

public class ProjectRemoveByIdWithTasksCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-id-with-tasks";
    }

    @Override
    public String description() {
        return "delete project with all its tasks.";
    }

    @Override
    public void execute() {
        System.out.println("Project Removal:");
        System.out.println("Enter Project Id:");
        final String projectId = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectTaskService().removeProjectWithTasksById(projectId);
        if (project == null) throw new ProjectNotFoundException();
        System.out.println("Project was successfully removed");
    }

}
