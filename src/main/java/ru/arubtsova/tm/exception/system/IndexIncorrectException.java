package ru.arubtsova.tm.exception.system;

import ru.arubtsova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(final String value) {
        super("Error! Entered value ``" + value + "`` is not a number...");
    }

    public IndexIncorrectException() {
        super("Error! Index is incorrect...");
    }

}
